#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 11:05:04 2022

@author: Jules Saur

This file assemble all rules used to detect potential errors
within a Base Adresse Locale file.
"""

#-----------
# Imports
#-----------

import re


#-----------
# Functions
#-----------

    # ------------
    # Ponctuation
    # ------------
    
def forbidden_punctuation(label): 
    """
    Return True if a punctuation is not in the authorized punctuation list.
    Authorized punctuation : 
    - tiret -
    - apostrophe ’ ou '
    - point . 
    """
    pattern = re.compile(r"[^\w\s’\-\'\.]")
    
    if re.search(pattern, label.replace(' ','')):
        return True
    else : 
        return False
    
    
def trailing_point(label):
    """
    Return True if the label ends with a point.
    """
    pattern =  re.compile(r'\.$')
    if re.search(pattern, label.replace(' ','')):
        return True
    else : 
        return False


def parentesis(label):
    """
    Return True if the label have something inside parenthesis (xxx).
    """
    pattern = re.compile(r'\(.+?\)')
    if re.search(pattern, label.replace(' ','')):
        return True
    else : 
        return False
    
    # ----------
    # Syntaxe
    # ----------

def contains_ovni(label, ovni):
    """
    Return True if the label have an ovni.
    An ovni can be any arbitrary string
    """
    if re.search(ovni, label, flags=re.I):
        return True
    else : 
        return False
    
def all_caps(label):
    """
    Return True if the label is in ALL CAPS
    """
    pattern = re.compile("^(?!.*[a-z\d]).+$")
    if re.search(pattern, label.replace(' ','')):
        return True
    else : 
        return False
    
def all_lowers(label):
    """
    Return True if the label is in all lower.
    """
    pattern = re.compile("^(?!.*[A-Z\d]).+$")
    if re.search(pattern, label.replace(' ','')):
        return True
    else : 
        return False
    

    




