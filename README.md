# bal-cop
## Identification d'erreurs courantes dans une Base Adresse Locale

Ce dépot permet d'identifier dans un fichier au format BAL 1.3 différentes erreurs pré-définies.
L'objectif étant d'automatiser un retour sur la qualité de la donnée adresse.    
  
Il diffère du [validateur-bal](https://github.com/BaseAdresseNationale/validateur-bal) qui est conçu pour établir la validité d'un fichier csv avec le format BAL (ex : clé d'interopérabilité conforme, champs vides, encodage en utf-8, etc.).   
  
Les *erreurs* qui sont constatés ne sont pas forcément de véritables *erreurs* et peuvent découler d'une manière d'adresser tout à fait légitime et cohérente.     

## La liste des erreurs :   

**Analyse syntaxique du champ `voie_nom` et `lieu_dit_complement_nom`**  

- Déjà implémenté :   
    - libellé tout en majuscule  
    - libellé tout en minuscule  
    - ponctuation interdite dans le libellé  
    - parenthèses dans le libellé  
    - point à la fin du libellé  
    - ovni dans le libellé (ex : 'Lieu dit' dans la liste des voies, 'abd')  
  
- À implémenter :   
    - un mot tout en majuscule (en dehors des chiffres romains)  
    - un mot tout en minuscule (en dehors des *stop words*, ex : le,la,les,...)  
    - hameaux et lieux-dits dans la colonne `voie_nom`  
    - contient des abréviations  
    - ne contient pas de type de voies
  
**Analyse syntaxique du champ `suffixe`**  
  
- Déjà implémenté :   
 
- À implémenter :  
    - suffixes différents de bis, ter, .. et a,b,c, ..)  
    - suffixe ou b est un bis  
  
**Analyse de cohérence sur l'ensemble de la BAL**  
  
- Déjà implémenté :   
  
- À implémenter :   
	 - incohérence entre les lieux-dits définis en toponyme et ceux de `lieu_dit_complement_nom`
    - deficit d'adresse  
  
  
## Fonctionement  
On soumet un fichier BAL, il passe d'abord au validateur BAL.  
Il est ensuite étiqueté ligne par ligne avec les erreurs de syntaxe identifiées. Puis de façon globale pour les règles de cohérence.   
Enfin on récupère deux fichiers de sortie : le fichiers BAL enrichi avec un petit résumé des erreurs.  
  

![[./notebooks/bal-cop-diagram.svg]]  


## Utilisation  
Des fichiers BAL peuvent être analysés à partir d'un notebook qui suit le fonctionement expliqué ci-dessus.  
